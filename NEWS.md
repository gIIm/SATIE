# SATIE Changelog

## [1.1.1] - 2018-08-15

## Improvements
- A better Readme

## [1.1.0] - 2018-08-13

### New features
- Analysis/monitoring side-chain with example monitoring plugins
- _delay_ post-processor
- Execute a file upon SATIE server boot
- Enable/disable compilation of SATIE plugins at boot

### Improvements
- Updated OSC API documentation which is now rendered in html
- Added new audio generator
- Ability to load custom plugins from user-defined directory
- Revised documentation

### Bugfixes
- Proper handling of sceneClear
- Proper handling of freeing synths from IDE
- Fix to quark file

### Removals
- SAT specific plugins

## [1.0.1] - 2018-04-19
### New features
- Support for ambisonics via SC-HOA quark
- Several mappers for each spatialiser
- Far and near field mappers

### Improvements
- Updated documentation
- Various bugfixes
